## Avaliação Luan Placido Coelho

Olá pessoal! Primeiramente quero agradecer a oportunidade que me deram de participar desse desafio. Gostei muito deste desafio. Gostaria de pedir desculpas pela demora na entrega mas no final deu tudo certo :)

Na solução foi utilizado um pouco de DDD para separação das camadas, deixando separado a parte de infraestrutura na camada de Repository (acesso ao banco de dados e envio de email), camada de Serviços em Services, onde foi criado os serviço para a leitura do csv e o serviço para a persistência, listagem e exclusão do Employee. Também foram criado duas entidades, a Employee e uma entidade maior chamado Employees.

Tentei deixar o serviço de leitura de csv fácil para ser trocado por outro serviço. Um exemplo seria um serviço que leria um JSON. A interface responsável por este contrato é a ReaderEmployee.

Foram criado testes unitários para os serviços de login e o upload de Employee, tentando cobrir o código para evitar os erros e comportamentos inesperados.
No teste de integração notei que em algumas vezes o teste de envio de email apresentou uma instabilidade, mas tentando novamente foi.

Hoje o serviço é chamado através de uma API mas nada impede de ser utilizado via CLI. Os serviços já tratam as exceções além das validações da Request.

### Como rodar o desafio?

```shell
$ composer install

$ cp .env.example .env

$ php artisan key:gen

$ php artisan jwt:secret

$ touch database/database.sqlite

$ php artisan migrate

$ php artisan db:seed

$ php artisan serve
```

Teste de Integração

```shell
$ ./vendor/bin/codecept run api
```

Teste Unitário

```shell
$ php vendor/bin/phpunit
```
