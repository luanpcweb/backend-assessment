<?php
namespace App\Repository;

use App\Entity\Employee;
use App\Mail\NotifyEmployeeUpload;
use Illuminate\Support\Facades\Mail;


class EmployeeRepository
{

    public function save(Employee $employee)
    {

        $employeeModel = new \App\Employee();
        $countEmployees = $employeeModel->where('document', $employee->getDocument())->count();

        if($countEmployees > 0) {
            return $employeeModel->where('document', $employee->getDocument())
            ->update([
                'name' => $employee->getName(),
                'email' => $employee->getEmail(),
                'city' => $employee->getCity(),
                'state' => $employee->getState(),
                'start_date' => $employee->getStartDateString(),
                'manager_id' => $employee->getManagerId(),
            ]);
        }

        return $employeeModel->create([
            'id' => $employee->getId(),
            'name' => $employee->getName(),
            'email' => $employee->getEmail(),
            'document' => $employee->getDocument(),
            'city' => $employee->getCity(),
            'state' => $employee->getState(),
            'start_date' => $employee->getStartDateString(),
            'manager_id' => $employee->getManagerId(),
            'created_at' => $employee->getCreatedAtString()
        ]);

    }

    public function list()
    {
        return \App\Employee::select(
            'name',
            'email',
            'document',
            'city',
            'state',
            'start_date',
            'manager_id'
        )->get();
    }

    public function destroyByDocument($document)
    {
        $employee = \App\Employee::where('document', $document);
        return $employee->delete();
    }

    public function notifyUploadByEmail($manager)
    {
        Mail::to($manager->email)->send(new NotifyEmployeeUpload);
    }
}
