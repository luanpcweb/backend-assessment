<?php

namespace App\Exceptions;

use UnexpectedValueException;

class HeaderLengthNotExpectedException extends UnexpectedValueException
{
}
