<?php
namespace App\Exceptions;

use UnexpectedValueException;

class HeaderNotExpectedException extends UnexpectedValueException
{

}
