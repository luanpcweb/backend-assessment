<?php
namespace App\Entity;

class Employees implements \Countable, \Iterator
{
    private $employees = [];
    private $position = 0;

    public function addEmployee(Employee $employee)
    {
        $this->employees[] = $employee;
    }

    public function addEmployees(Employees $employees)
    {
        $this->employees = array_merge(
            $this->employees,
            $employees->getEmployeesAsArray()
        );
    }

    public function count()
    {
        return count($this->employees);
    }

    public function current(): Employee
    {
        return $this->employees[$this->position];
    }

    public function key()
    {
        return $this->position;
    }

    public function next()
    {
        return ++$this->position;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function valid()
    {
        return isset($this->employees[$this->position]);
    }

    public function getEmployeesAsArray(): array
    {
        return $this->employees;
    }

    public function contains(Employee $employee): bool
    {
        foreach ($this->employees as $internalEmployee) {
            if($internalEmployee->toHashString() === $employee->toHashString()) {
                return true;
            }
        }

        return false;
    }
}
