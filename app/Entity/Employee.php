<?php

namespace App\Entity;

class Employee
{
    private $id;
    private $name;
    private $email;
    private $document;
    private $city;
    private $state;
    private $startDate;
    private $managerId;
    private $createdAt;

    public function __construct(
        string $id,
        string $name,
        string $email,
        string $document,
        string $city,
        string $state,
        \DateTime $startDate,
        string $managerId,
        \DateTime $createdAt
    )
    {
        $this->id = $id;
        $this->name = $name;
        $this->email = $email;
        $this->document = $document;
        $this->city = $city;
        $this->state = $state;
        $this->startDate = $startDate;
        $this->managerId = $managerId;
        $this->createdAt = $createdAt;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getDocument(): string
    {
        return $this->document;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function getState(): string
    {
        return $this->state;
    }

    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    public function getStartDateString(): string
    {
        return $this->startDate->format('Y-m-d');
    }

    public function setManagerId($managerId): void
    {
        $this->managerId = $managerId;
    }

    public function getManagerId(): string
    {
        return $this->managerId;
    }

    public function toHashString()
    {
        return sprintf('%s-%s-%s-%s', $this->name, $this->email, $this->document, $this->email);
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getCreatedAtString(): string
    {
        return $this->createdAt->format('Y-m-d H:i:s');
    }
}
