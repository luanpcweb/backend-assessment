<?php

namespace App;

use App\Entity\Employees;

interface ReaderEmployee
{
    public function __construct(string $source, array $expectedHeader);
    public function read(): Employees;
}
