<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $table = 'employees';

    protected $fillable = ['id', 'name', 'email', 'document', 'city', 'state', 'start_date', 'manager_id'];

    protected $keyType = 'string';
}
