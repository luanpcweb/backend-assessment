<?php
namespace App\Service;

use App\Entity\Employee;
use App\Entity\Employees;
use App\ReaderEmployee;
use League\Csv\Reader;
use Ramsey\Uuid\Uuid;

use App\Exceptions\HeaderNotExpectedException;
use App\Exceptions\HeaderLengthNotExpectedException;

class CsvEmployeeService implements ReaderEmployee
{
    private $source;
    private $expectedHeader;

    public function __construct(string $source, array $expectedHeader)
    {
        $this->source = $source;
        $this->expectedHeader = $expectedHeader;
    }

    public function read(): Employees
    {

        $csv = Reader::createFromPath($this->source, 'r');
        $csv->setHeaderOffset(0);

        $header = $csv->getHeader();

        if (!$this->ensureIntegrityLengthHeader($header)) {
            throw new HeaderLengthNotExpectedException();
        }

        if (!$this->ensureIntegrityFields($header)) {
            throw new HeaderNotExpectedException();
        }

        $recordsCsv = $csv->getRecords();

        $now = new \Datetime('now');

        $employees = new Employees();
        foreach ($recordsCsv as $record) {

            $id = Uuid::uuid5(
                Uuid::NAMESPACE_URL,
                $record['name'],
                $record['email'],
                $now->format('Ymdims')
            );

            $employees->addEmployee(
                new Employee(
                    $id->toString(),
                    $record['name'],
                    $record['email'],
                    $record['document'],
                    $record['city'],
                    $record['state'],
                    new \DateTime($record['start_date']),
                    '',
                    $now
                )
            );
        }

        return $employees;

    }

    private function ensureIntegrityFields($header)
    {
        foreach ($header as $headerValue) {
            if(!in_array($headerValue, $this->expectedHeader)) {
                return false;
            }
        }

        return true;
    }

    private function ensureIntegrityLengthHeader($header)
    {

        if(count($this->expectedHeader) !== count($header)) {
            return false;
        }

        return true;
    }
}
