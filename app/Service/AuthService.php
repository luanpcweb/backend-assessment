<?php

namespace App\Service;
use App\Exceptions\InvalidEmailOrPasswordException;
use App\Exceptions\EmptyEmailOrPasswordException;
use Illuminate\Support\Facades\Auth;

class AuthService
{
    public function login($email, $password)
    {
        $accessToken = null;

        if(empty($email)) {
            throw new EmptyEmailOrPasswordException();
        }

        if (empty($password)) {
            throw new EmptyEmailOrPasswordException();
        }

        $credentials = [
            "email" => $email,
            "password" => $password
        ];

        if (!$accessToken = Auth::attempt($credentials)) {
            throw new InvalidEmailOrPasswordException();
        }

        return $accessToken;
    }

}
