<?php
namespace App\Service;

use App\Entity\Employee;
use App\Entity\Employees;
use App\Repository\EmployeeRepository;
use App\Exceptions\EmptyEmployeesException;
use App\Exceptions\EmptyEmployeeDocument;


class EmployeeService
{
    private $employeeRepository;

    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    public function list()
    {
        return $this->employeeRepository->list();
    }

    public function save(Employees $employees, $manager)
    {
        $managerId = $manager->id;

        if($employees->count() == 0) {
            throw new EmptyEmployeesException();
        }

        foreach ($employees as $employee) {

            $employee->setManagerId($managerId);

            $this->employeeRepository->save($employee);
        }

        $this->employeeRepository->notifyUploadByEmail($manager);
    }

    public function destroyByDocument($document)
    {
        if(empty($document)){
            throw new EmptyEmployeeDocument();
        }

        return $this->employeeRepository->destroyByDocument($document);
    }
}
