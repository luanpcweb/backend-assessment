<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostEmployeeRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Repository\EmployeeRepository;
use App\Service\EmployeeService;
use App\Service\CsvEmployeeService;
use App\Exceptions\HeaderLengthNotExpectedException;
use App\Exceptions\HeaderNotExpectedException;
use App\Exceptions\EmptyEmployeesException;
use App\Exceptions\EmptyEmployeeDocument;


class EmployeeController extends Controller
{
    private $employeeRepository;

    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->middleware('auth:api');

        $this->employeeRepository = $employeeRepository;
    }

    public function list()
    {
        $employeeService = new EmployeeService($this->employeeRepository);
        return $employeeService->list();
    }

    public function post(PostEmployeeRequest $request)
    {

        try {

            $fileName = time() . '.' . $request->file('employees')->extension();
            $path = public_path('uploads');
            $request->file('employees')->move($path, $fileName);
            $file = $path . '/' . $fileName;

            $expectedHeader = ['name', 'email', 'document', 'city', 'state', 'start_date'];
            $manager = auth()->user();

            $readerEmployee = new CsvEmployeeService($file, $expectedHeader);
            $employees = $readerEmployee->read();

            $employeeService = new EmployeeService($this->employeeRepository);
            $employeeService->save($employees, $manager);

            return response()->json(['message' => 'Created'], Response::HTTP_CREATED);

        } catch(HeaderLengthNotExpectedException $e) {

            return response()->json([
                'message' => 'Invalid header length'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);

        } catch (HeaderNotExpectedException $e) {

            return response()->json([
                'message' => 'Invalid header Field'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);

        } catch(EmptyEmployeesException $e) {
            return response()->json([
                'message' => 'Empty Employees'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);

        } catch(\Exception $e) {
            return response()->json([
                'message' => 'Error Registering'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function destroyByDocument($document)
    {
        try {

            $employeeService = new EmployeeService($this->employeeRepository);
            $employeeService->destroyByDocument($document);
            return response()->json([], Response::HTTP_NO_CONTENT);

        } catch(EmptyEmployeeDocument $e) {
            return response()->json([
                'message' => 'Empty employee document'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
