<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Service\AuthService;
use App\Http\Requests\AuthLoginRequest;
use Symfony\Component\HttpFoundation\Response;

use App\Exceptions\InvalidEmailOrPasswordException;
use App\Exceptions\EmptyEmailOrPasswordException;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    public function login(AuthLoginRequest $request)
    {
        $accessToken = null;
        $email = $request->email;
        $password = $request->password;

        try {

            $authService = new AuthService();
            $accessToken = $authService->login($email, $password);
            return response()->json([
                'access_token' => $accessToken
            ]);

        } catch(EmptyEmailOrPasswordException $e) {

            return response()->json([
                'message' => 'Empty Email or Password'
            ], Response::HTTP_UNAUTHORIZED);

        } catch (InvalidEmailOrPasswordException $e) {

            return response()->json([
                'message' => 'Invalid Email or Password'
            ], Response::HTTP_UNAUTHORIZED);
        }

        return response()->json([
            'message' => 'Could not create token'
        ], Response::HTTP_UNAUTHORIZED);
    }

}
