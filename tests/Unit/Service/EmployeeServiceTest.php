<?php

namespace Tests\Unit\Service;

use App\Entity\Employees;
use App\Entity\Employee;
use App\Repository\EmployeeRepository;
use App\Service\EmployeeService;
use App\Exceptions\EmptyEmployeesException;
use App\Exceptions\EmptyEmployeeDocument;

use PHPUnit\Framework\TestCase;

class EmployeeServiceTest extends TestCase
{

    private $employeeRepository;

    public function setUp(): void
    {
        $this->employeeRepository = $this->getMockBuilder(EmployeeRepository::class)
            ->getMock();

    }

    /**
     * @test
     */
    public function shouldPersistEmployeeOnDatabase()
    {

        $authMock = $this->getMockBuilder(\Illuminate\Contracts\Auth\Factory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $authMock->id = 1;
        $authMock->email = 'manager@email.com';

        $employee1 = new Employee(
            'bd967d428e20f7655671a705ff471152',
            'Name',
            'test@test.com',
            '000.000.000-00',
            'Redenção',
            'Pa',
            new \DateTime(),
            '',
            new \DateTime(),
        );

        $employees = new Employees();
        $employees->addEmployee($employee1);

        $this->employeeRepository->expects($this->once())->method('save');

        $employee = new EmployeeService($this->employeeRepository);
        $employee->save($employees, $authMock);
    }

    /**
     * @test
     */
    public function shouldNotifyUploadByEmail()
    {

        $authMock = $this->getMockBuilder(\Illuminate\Contracts\Auth\Factory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $authMock->id = 1;
        $authMock->email = 'manager@email.com';

        $employee1 = new Employee(
            'bd967d428e20f7655671a705ff471152',
            'Name',
            'test@test.com',
            '000.000.000-00',
            'Redenção',
            'Pa',
            new \DateTime(),
            '',
            new \DateTime(),
        );

        $employees = new Employees();
        $employees->addEmployee($employee1);

        $this->employeeRepository->expects($this->once())->method('notifyUploadByEmail');

        $employee = new EmployeeService($this->employeeRepository);
        $employee->save($employees, $authMock);
    }

    /**
     * @test
     */
    public function shouldNotPersistEmptyEmployeeOnDatabase()
    {

        $authMock = $this->getMockBuilder(\Illuminate\Contracts\Auth\Factory::class)
            ->disableOriginalConstructor()
            ->getMock();
        $authMock->id = 1;
        $authMock->email = 'manager@email.com';

        $this->expectException(EmptyEmployeesException::class);

        $employees = new Employees();

        $employee = new EmployeeService($this->employeeRepository);
        $employee->save($employees, $authMock);
    }

    /**
     * @test
     */
    public function shouldNotDestroyEmployeeOnDatabase()
    {

        $this->expectException(EmptyEmployeeDocument::class);

        $document = '';

        $employee = new EmployeeService($this->employeeRepository);
        $employee->destroyByDocument($document);
    }

    /**
     * @test
     */
    public function shouldDestroyEmployeeOnDatabase()
    {
        $this->employeeRepository->expects($this->once())->method('destroyByDocument');

        $document = '000.000.000-00';

        $employee = new EmployeeService($this->employeeRepository);
        $employee->destroyByDocument($document);
    }


}
