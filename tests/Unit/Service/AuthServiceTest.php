<?php
namespace Tests\Unit\Service;

use App\Service\AuthService;
use App\Exceptions\EmptyEmailOrPasswordException;
use App\Exceptions\InvalidEmailOrPasswordException;

use PHPUnit\Framework\TestCase;

class AuthServiceTest extends TestCase
{

    /**
     * @test
     */
    public function shouldFailedLoginWithoutEmail()
    {

        $this->expectException(EmptyEmailOrPasswordException::class);

        $email = '';
        $password = 'C0nv3n!4';

        $authService = new AuthService();
        $authService->login($email, $password);
    }

    /**
     * @test
     */
    public function shouldFailedLoginWithoutPassword()
    {

        $this->expectException(EmptyEmailOrPasswordException::class);

        $email = 'dennis.ritchie@convenia.com.br';
        $password = '';

        $authService = new AuthService();
        $authService->login($email, $password);
    }

    /**
     * @test
     */
    public function shouldFailedLoginInvalidEmailOrPassword()
    {

        $authMock = \Mockery::mock('alias:Illuminate\Support\Facades\Auth');
        $authMock->shouldReceive('attempt')->andReturns(false);

        $this->expectException(InvalidEmailOrPasswordException::class);

        $email = 'dennis.ritchie@convenia.com.br';
        $password = '12345678';

        $authService = new AuthService();
        $authService->login($email, $password);
    }


}
