<?php

namespace Tests\Unit\Service;

use App\Entity\Employee;
use App\Service\CsvEmployeeService;
use App\Exceptions\HeaderLengthNotExpectedException;
use App\Exceptions\HeaderNotExpectedException;

use PHPUnit\Framework\TestCase;

class CsvEmployeeServiceTest extends TestCase
{

    /**
     * @test
     */
    public function shouldFailedReadCsvWithHeaderLengthWrong()
    {
        $source = __DIR__.'/../../_data/employees.csv';
        $expectedHeader = ['name','email','document','city','state'];

        $this->expectException(HeaderLengthNotExpectedException::class);

        $csv = new CsvEmployeeService($source, $expectedHeader);
        $csv->read();

    }

    /**
     * @test
     */
    public function shouldFailedReadCsvWithHeaderNotExpected()
    {
        $source = __DIR__ . '/../../_data/employees.csv';
        $expectedHeader = ['name', 'email', 'document', 'city', 'state', 'start_date_wrong'];

        $this->expectException(HeaderNotExpectedException::class);

        $csv = new CsvEmployeeService($source, $expectedHeader);
        $csv->read();
    }


    /**
     * @test
     */
    public function shouldSuccessReadCsv()
    {
        $source = __DIR__ . '/../../_data/employees.csv';
        $expectedHeader = ['name', 'email', 'document', 'city', 'state', 'start_date'];

        $csv = new CsvEmployeeService($source, $expectedHeader);
        $employees = $csv->read();

        $countBySources = 9; // count employees.csv

        $this->assertEquals($countBySources, $employees->count());
    }


}
